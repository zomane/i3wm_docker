Running i3-wm(or any window manager/desktop environment) in docker.
Based on http://blog.csicar.de/docker/window-manger/2016/05/24/docker-wm.html

running just a single GUI app can be done the existing X session but 
for WM or DE is better to use another virtual diplay

```
apt-get install xserver-xephyr
Xephyr :1 -ac -br -screen 1360x768 -resizeable -reset -terminate &
```
cd in the repo:
```
docker build -t itri .
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix  -e DISPLAY=:1 itri i3
```

