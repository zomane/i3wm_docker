FROM debian
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt install -y xserver-xorg i3 i3status
RUN mkdir -p /root/.config/i3
COPY config /root/.config/i3/ 
#CMD ["/usr/bin/i3"]
